# -*- coding: utf-8 -*-
"""BSE-IT-BUY-Screener.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1mPj3FkH_r2imFNLoGuxCC6bvh7U_HAba
"""

# Commented out IPython magic to ensure Python compatibility.
from prettytable import PrettyTable
import pandas as pd
import pandas_datareader as pdr
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
import traceback
import sys
# %matplotlib inline

#f = open("bse-stocks.txt", "r")
#symbols = f.read().split("\n")
symbols = ['%5ENSEI','TCS.BO','LTI.BO']
print(symbols)

class Instrument:
  symbol = None
  highDate = None
  highPrice= None
  lowDate= None
  lowPrice= None
  currentDate= None
  currentPrice= None

  def __init__(self, symbol):
    self.symbol = symbol

  def getHighCurrentPriceChange(self):
    return round(currentPrice - highValPrice,2)

  def getPctHighCurrentPriceChange(self):
    return round(100*self.getHighCurrentPriceChange()/highValPrice,2)

  def asPrittyTable(self):
    highLowChange = round(lowValPrice - highValPrice,2)
    pctHighLowChange = round(100*highLowChange/highValPrice,2)

    highCurrentPriceChange = self.getHighCurrentPriceChange()
    pctHighCurrentPriceChange = self.getPctHighCurrentPriceChange()

    return [self.symbol,self.highDate,self.highPrice,self.lowDate,self.lowPrice,highLowChange, pctHighLowChange, self.currentDate,self.currentPrice, highCurrentPriceChange, pctHighCurrentPriceChange]
    
  @staticmethod
  def PrittyTable():
    return PrettyTable(['Symbol','HighDate', 'HighPrice','LowDate', 'LowPrice','HighLowChange','%HighLowChange', 'CurrentDate', 'CurrentPrice','HighCurrChange','%HighCurrChange'])

  def __str__(self):
    table = Instrument.PrittyTable()
    #PrettyTable(['Symbol','HighDate', 'HighPrice','LowDate', 'LowPrice','HighLowChange','%HighLowChange', 'CurrentDate', 'CurrentPrice','HighCurrChange','%HighCurrChange'])
    table.add_row(self.asPrittyTable())
    return table.get_string(sortby="Symbol")

table = Instrument.PrittyTable()

print(table.get_string())
for symbol in symbols:
    try:
      #print("Fetching ", symbol)
      historicalData = pdr.DataReader(symbol, "yahoo", 
                          datetime(2021, 2, 2), 
                          datetime(2021, 12, 31))

      historicalData.drop(columns=['Open', 'Volume', 'Adj Close'], inplace=True)

      historicalData.dropna(inplace=True)
      #historicalData.tail(10)

      currentDate = historicalData.tail(1).index[0].strftime('%Y-%m-%d')
      currentPrice = round(historicalData.tail(1)['Close'][0],2)

      highValRow = historicalData.loc[historicalData['High'].idxmax()]
      lowValRow = historicalData.loc[historicalData['Low'].idxmin()]

      highValPrice = round(highValRow.get("High"),2)
      highValDate = highValRow.name.strftime('%Y-%m-%d')
      lowValPrice = round(lowValRow.get("Low"),2)
      lowValDate = lowValRow.name.strftime('%Y-%m-%d')

      # print("*"*30, "Low Val", "*"*30)
      # print("CurrDate:",currentDate)
      # print("CurrPrice", currentPrice)
      # print("-"*70)


      # print("*"*30, "High Val", "*"*30)
      # print(highValRow)
      # print("-"*70)
      # print("*"*30, "Low Val", "*"*30)
      # print(lowValRow)

      #print(highValDate,",", highValPrice)
      #print(lowValDate,",", lowValPrice)
      #print("-"*70)

      if(lowValRow.name >= highValRow.name):
        table1 = Instrument.PrittyTable()
        instrument = Instrument(symbol)
        instrument.highDate = highValDate
        instrument.highPrice = highValPrice
        instrument.lowDate = lowValDate
        instrument.lowPrice = lowValPrice
        instrument.currentDate = currentDate
        instrument.currentPrice = currentPrice
        #diffDays = str((lowValRow.name.date()  - highValRow.name.date()).days)+" Days"
        
        row = instrument.asPrittyTable()
        table.add_row(row)
        table1.add_row(row)
        print(table1.get_string(sortby="CurrentPrice", header = False, left_padding_width=4))
        
        
 
    except:
      print("could not able to fetch", symbol)
      traceback.print_exception(*sys.exc_info())
      sys.exit("Some exception occurred.")
      


print(table.get_string(sortby="CurrentPrice", reversesort=True))

print(table.get_csv_string(sortby="CurrentPrice", reversesort=True))


