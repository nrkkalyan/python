import pandas as pd
import numpy as np
import pandas_datareader as pdr
from datetime import datetime
from prettytable import PrettyTable


def calculatePctReturn(v1, v2):
    return round(v2.sub(v1).div(v1).mul(100), 2)

def calculatePctReturn(v1:float, v2:float):
    return round(100*(v2 - v1)/(v1), 2)


def getDataFromYahoo(symbol, fromDate, toDate):
    if (toDate == None):
        toDate = datetime.now().strftime("%Y%m%d")
    else:
        toDate = datetime.strptime(toDate, "%Y%m%d")

    df = pdr.get_data_yahoo(symbol, start=fromDate, end=toDate)
    df.insert(1, "Date", datetime.strftime(toDate, "%Y-%m-%d"))
    # print(df.tail())
    df.drop(columns=['Open', 'High', 'Low', 'Volume', 'Adj Close'], inplace=True)
    df['Close'] = round(df.Close)

    dfLen = len(df)
    sma = df['Close'].rolling(dfLen).mean()
    rstd = df['Close'].rolling(dfLen).std()

    upper_band = round(sma + 2 * rstd)
    df['%upband'] = calculatePctReturn(df['Close'], upper_band)

    lower_band = round(sma - 2.2 * rstd)
    df['%lowband'] = calculatePctReturn(df['Close'], lower_band)

    mid_band = (upper_band + lower_band) / 2
    df['%midband'] = calculatePctReturn(df['Close'], mid_band)

    #df.dropna(inplace=True)
    for x in range(1, 20):
        curClose = round(df['Close'])
        prevDate = curClose.tail(x).index[0].strftime("%Y-%m-%d")

        curValStr = curClose.get(toDate).__str__()
        preValStr = curClose.get(prevDate).__str__()
        pctChange = calculatePctReturn(float(preValStr), float(curValStr))

        if(x >1):
            df[prevDate] = str(pctChange) + "(" +preValStr+ ")"

    df.dropna(inplace=True)
    return df


# pd.set_option('display.max_columns', None)
# pd.set_option('display.max_rows', None)

print("-" * 100)
fromDate = '20210310'
toDate = '20210322'


stockDataRecords = pd.DataFrame
table = PrettyTable(['Symbol', 'Close'])
isTableDefined = 0

for symbol in ['%5ENSEI']:

    print("Fetching data for ", symbol)

    df = getDataFromYahoo(symbol, fromDate, toDate)
    df.insert(0, "Symbol", symbol)

    if (isTableDefined == 0):
        table = PrettyTable(list(df.columns))
        isTableDefined = 1

    #print(df.tail(1).values[0])
    table.add_row(df.tail(1).values[0])

# print(stockDataRecords)

print("-" * 100)
print(table.get_string(sortby="Symbol"))

# Find all stocks that are close to the lower band.
