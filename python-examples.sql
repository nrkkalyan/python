CREATE TABLE StockHistory (
    Date  DATE,
    Stock TEXT,
    High  FLOAT,
    Low   FLOAT,
    Close FLOAT
);

CREATE UNIQUE INDEX ix_StockHistory_Date ON StockHistory ( Date, Stock );

