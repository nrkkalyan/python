import requests
import sqlite3
import traceback
import sys

def saveInstumentsToDB():

    con = sqlite3.connect('/Users/nrkkalyan/PycharmProjects/mypython-1/screener/data/db/screener.db')
    cur = con.cursor()
    stockSymbolsFile = "/Users/nrkkalyan/PycharmProjects/mypython-1/screener/data//stocksymbols.txt"
    try:
        f = open(stockSymbolsFile, "r")
        for x in f:
            stkSmbl = x.rstrip()
            screener_url = 'https://www.screener.in/api/company/search/?q=' + stkSmbl
            content = requests.get(screener_url)
            for s in content.json():
                id = s['id']
                name = s['name']
                symbol = s['url'].replace("/", "").replace("company", "").replace("consolidated", "")
                x = id, name, symbol
                # print(x)
                if(stkSmbl == symbol):
                    try:
                        cur.execute("Insert into Instruments(id,name,stock) values(?,?,?)", x)
                        print(id, ",", name, ",", symbol, " Record inserted successfully.")
                    except:
                        traceback.print_exception(*sys.exc_info())
                        sys.exit("Some exception occurred.")
    finally:
        con.commit()
        con.close()

print("Searching and saving Instruments to DB");
saveInstumentsToDB();
